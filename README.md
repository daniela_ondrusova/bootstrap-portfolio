# Bootstrap-Portfolio

**Features**

- Gebruik van strakke Bootstrap-styling.
- Volledig responsive design.
- Overzichtelijke navigatiebalk voor eenvoudige toegang.
- Dynamische sectie voor het tonen van vaardigheden met stijlvolle progressiebalken.
- Projectsectie met beknopte samenvattingen en afbeeldingen.
- Contactsectie voor moeiteloos contact.

*Inline styling toegestaan voor progressiebalken.*


U kunt Bootstrap-Portfolio [hier](https://preeminent-croissant-c0b09c.netlify.app/) vinden.